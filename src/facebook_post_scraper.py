import argparse
import json
import random
import sys
import time
import warnings

import settings
from scrapers.middlewares import ScraperMiddleware
from scrapers.pipelines import WebservicePipeline
from scrapers.scheduler import Scheduler
from utils.io import DataIO

class FacebookPostScraper:
    def __init__(self, scraper_arguments):
        """
        Initialize the FacebookPostScraper with scraper_arguments arguments.

        Args:
            scraper_arguments (dict): A dictionary containing arguments and configurations for the scraper.
        """
        # The __init__ method is a constructor used to initialize
        # an instance of the FacebookPostScraper class.
        # It takes a dictionary `scraper_arguments` as an argument, which typically
        # contains various settings and parameters needed for scraping Facebook posts.
        self.scraper_arguments = scraper_arguments

    def run(self):
        try:
            # Create an instance of the Scheduler class with the provided scraper arguments.
            scheduler = Scheduler(self.scraper_arguments)

            # Create an instance of the ScraperMiddleware class with the provided scraper arguments.
            scraper_middleware = ScraperMiddleware(self.scraper_arguments)

            # Create an instance of the WebservicePipeline class with the provided scraper arguments.
            webservice_pipeline = WebservicePipeline(self.scraper_arguments)
            
            # Initialize the generator for Facebook post URLs
            facebook_post_urls_generator = scheduler.initialize_post_urls()
            
            for index, facebook_post_url in enumerate(facebook_post_urls_generator):
                DataIO().write_to_log(f'{index + 1}: {facebook_post_url}')
                
                # Process the Facebook post URL
                post_info = scraper_middleware.process_facebook_post(facebook_post_url)
                
                # Convert post_info to a formatted JSON string
                data_string = json.dumps(post_info, indent=4)
                
                # Write the JSON data to a log file
                DataIO().write_to_log(data_string)
                
                # Push the post_info to a web service
                webservice_pipeline.push_to_webservice(post_info)
                
                # Sleep for a random duration between 10 to 20 seconds
                time.sleep(random.randint(10, 20))
        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)

if __name__ == '__main__':
    # Suppress warnings to prevent them from cluttering the output.
    warnings.filterwarnings('ignore')

    # Parse command line arguments (currently not used)
    parser = argparse.ArgumentParser()
    scraper_arguments = parser.parse_args()

    # Enter an infinite loop to run the scraper continuously.
    while True:
        try:
            # Get the current date and time using the DataIO class.
            current_time = DataIO().get_current_time()

            # Uncomment the following condition to execute the scraper logic at a specific time (e.g., midnight).
            # if (current_time.hour == 0 and current_time.minute == 0 and current_time.second == 0):
            if True:
                # Set the 'current_time' attribute of 'scraper_arguments' to the current time.
                setattr(scraper_arguments, 'current_time', current_time)

                # Create an instance of the FacebookPostScraper class with 'scraper_arguments'.
                facebook_post_scraper = FacebookPostScraper(scraper_arguments)

                # Run the scraper logic by invoking the 'run' method of the FacebookPostScraper.
                facebook_post_scraper.run()
        except Exception as e:
            # Log any exceptions that occur during execution.
            error_message = f'An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)

        # Sleep for a specified repeat interval (in minutes) before the next iteration.
        time.sleep(settings.REPEAT_INTERVAL)
