import platform
import sys
import urllib3
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import settings
from utils.io import DataIO

# Disable urllib3 warnings about insecure requests
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class Downloader:
    """
    Downloader class responsible for downloading data during web scraping.
    """

    def __init__(self, scraper_arguments):
        """
        Initialize the Downloader.

        Args:
            scraper_arguments (dict): Scraper arguments.
        """
        self.scraper_arguments = scraper_arguments

    def initialize_chrome_driver(self):
        """
        Initialize a Chrome WebDriver with specified options.

        Returns:
            WebDriver: Chrome WebDriver instance.
        """
        chrome_driver = None
        try:
            chrome_options = Options()

            # Handle various Chrome options based on settings

            # Option: Ignore certificate errors
            if settings.IGNORE_CERTIFICATE_ERRORS:
                chrome_options.add_argument('--ignore-certificate-errors')

            # Option: Ignore SSL errors
            if settings.IGNORE_SSL_ERRORS:
                chrome_options.add_argument('--ignore-ssl-errors')

            # Option: Run in headless mode (without a graphical interface)
            if settings.HEADLESS:
                chrome_options.add_argument("--headless")

            # Option: Set Chrome log level
            if settings.LOG_LEVEL:
                chrome_options.add_argument(
                    '--log-level=' + str(settings.LOG_LEVEL))

            # Option: Disable sandbox mode
            if settings.NO_SANDBOX:
                chrome_options.add_argument('--no-sandbox')

            # Option: Disable /dev/shm usage
            if settings.DISABLE_DEV_SHM_USAGE:
                chrome_options.add_argument('--disable-dev-shm-usage')

            # Option: Set Chrome language code
            if settings.CHROME_LANGUAGE_CODE:
                chrome_options.add_argument('--lang=en-US')
                chrome_options.add_argument('--lang=en-GB')
                chrome_options.add_experimental_option(
                    'prefs', {'intl.accept_languages': settings.CHROME_LANGUAGE_CODE})

            # Option: Disable automation
            if settings.ENABLE_AUTOMATION:
                chrome_options.add_experimental_option(
                    "excludeSwitches", ['enable-automation'])

            # Option: Disable automation extension
            if settings.DISABLE_AUTOMATION_EXTENSION:
                chrome_options.add_experimental_option(
                    'useAutomationExtension', False)

            # Option: Use custom user agent
            if settings.USE_COOKIE:
                experimental_flags = [
                    'same-site-by-default-cookies@1', 'cookies-without-same-site-must-be-secure@1']
                chromeLocal_state_prefs = {
                    'browser.enabled_labs_experiments': experimental_flags}
                chrome_options.add_experimental_option(
                    'localState', chromeLocal_state_prefs)

            # Option: Use custom user agent
            if settings.USE_USER_AGENT:
                user_agent = DataIO().generate_user_agent()
                chrome_options.add_argument('--user-agent=%s' % user_agent)

            # Option: Disable JavaScript
            if settings.DISABLE_JAVASCRIPT:
                chrome_options.add_argument("--disable-javascript")
                chrome_options.add_experimental_option("prefs", {
                    'profile.managed_default_content_settings.javascript': 2,
                    "webkit.webprefs.javascript_enabled": False,
                    "profile.content_settings.exceptions.javascript.*.setting": 2,
                    "profile.default_content_setting_values.javascript": 2
                })

            # Option: Enable detachment
            if settings.DETACH:
                chrome_options.add_experimental_option("detach", True)

            # Initialize Chrome WebDriver based on the platform
            if platform.system() == 'Windows':
                chrome_driver = webdriver.Chrome(
                    settings.DEFAULT_WINDOWS_CHROME_DRIVER_PATH, chrome_options=chrome_options)
            elif platform.system() == 'Linux':
                chrome_driver = webdriver.Chrome(
                    settings.DEFAULT_UBUNTU_CHROME_DRIVER_PATH, chrome_options=chrome_options)

            # Set page load timeout
            if settings.PAGE_LOAD_TIMEOUT:
                chrome_driver.set_page_load_timeout(settings.PAGE_LOAD_TIMEOUT)

            # Set implicit wait timeout
            if settings.IMPLICITLY_WAIT:
                chrome_driver.implicitly_wait(settings.IMPLICITLY_WAIT)

        except Exception as e:
            current_class = self.__class__.__name__
            # Log any exceptions that occur during initialization
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return chrome_driver

    def close_chrome_driver(self, chrome_driver):
        """
        Close the Chrome WebDriver.

        Args:
            chrome_driver (WebDriver): Chrome WebDriver instance to be closed.

        Returns:
            None
        """
        try:
            # Check if the Chrome driver instance has a 'close' method before calling it
            if hasattr(chrome_driver, 'close') and callable(chrome_driver.close):
                chrome_driver.close()
        except Exception as e:
            current_class = self.__class__.__name__
            # Log any exceptions that occur during WebDriver closure
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
