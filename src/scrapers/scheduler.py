import random
import sys
import requests
from requests.exceptions import RequestException
import settings
from utils.io import DataIO

class Scheduler:
    """
    Scheduler class responsible for initializing and providing Facebook post URLs.
    """

    def __init__(self, scraper_arguments):
        """
        Initialize the Scheduler.

        Args:
            scraper_arguments (dict): Scraper arguments.
        """
        self.scraper_arguments = scraper_arguments

    def initialize_post_urls(self):
        """
        Initialize and yield Facebook post URLs.

        Yields:
            str: A Facebook post URL.
        """
        try:
            facebook_post_urls = list()

            # Create the URL for retrieving Facebook post URLs from the API.
            api_facebook_post_url = settings.HOSTNAME + settings.API_FACEBOOK_POST_PATH

            # Send a GET request to retrieve post URLs.
            http_response = requests.get(
                api_facebook_post_url, verify=False, timeout=settings.TIMEOUT)

            # Parse the JSON response.
            json_response = http_response.json()

            # Extract post URLs from the JSON response.
            facebook_post_urls = [entry['post_url'] for entry in json_response]

            # Shuffle the list of post URLs for randomization.
            random.shuffle(facebook_post_urls)

            # Iterate over the post URLs and prepare them for crawling.
            for facebook_post_url in facebook_post_urls:
                # Convert 'https://www.facebook.com' URLs to mobile format 'https://m.facebook.com'.
                facebook_post_url = facebook_post_url.replace(
                    'https://www.facebook.com', 'https://m.facebook.com')

                # Ensure URLs contain 'https://m'.
                if 'https://m' not in facebook_post_url:
                    facebook_post_url = facebook_post_url.replace(
                        'www.', str()).replace('https://', 'https://m.')

                # Yield the prepared URL for crawling.
                yield facebook_post_url

        except Exception as e:
            # Handle other exceptions.
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {sys.exc_info()[-1].tb_lineno}: {str(e)}'
            DataIO().write_to_log(error_message)
            yield
