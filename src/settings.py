# Define constants for scraper settings

# Timezone for date and time operations
TIMEZONE = 'Asia/Saigon'

# Global timeout for various operations in seconds
TIMEOUT = 600

# Use cookies for web scraping (True or False)
USE_COOKIE = True

# Use a custom user agent for web scraping (True or False)
USE_USER_AGENT = True

# Logging level for debugging (e.g., 1 for minimal logging, 3 for verbose logging)
LOG_LEVEL = 3

# Hostname of the web service
HOSTNAME = 'http://207.148.17.130'

# API endpoint for Facebook video post data
API_FACEBOOK_POST_PATH = '/api/v1/facebook-video-posts'

# URL to a distraction page (e.g., for simulating user behavior)
DISTRACTION_URL = 'https://www.google.com'

# Timeout for loading web pages in seconds
PAGE_LOAD_TIMEOUT = 120

# Implicit wait time for elements to appear on the web page in seconds
IMPLICITLY_WAIT = 120

# Request timeout for HTTP requests in seconds
REQUEST_TIMEOUT = 120

# Interval between repeating tasks (in minutes)
REPEAT_INTERVAL = 1

# Ignore certificate errors during web scraping (True or False)
IGNORE_CERTIFICATE_ERRORS = True

# Ignore SSL errors during web scraping (True or False)
IGNORE_SSL_ERRORS = True

# Run the web browser in headless mode (True or False)
HEADLESS = False

# Disable the use of a sandbox when running the browser (True or False)
NO_SANDBOX = True

# Disable the use of /dev/shm when running the browser (True or False)
DISABLE_DEV_SHM_USAGE = True

# Enable or disable browser automation (True or False)
ENABLE_AUTOMATION = True

# Disable browser extensions for automation (True or False)
DISABLE_AUTOMATION_EXTENSION = True

# Enable or disable JavaScript in the browser (True or False)
DISABLE_JAVASCRIPT = False

# Detach the browser process (True or False)
DETACH = True

# Language code for the Chrome browser (e.g., 'en', 'en_US')
CHROME_LANGUAGE_CODE = 'en,en_US'

# Default path to the Chrome WebDriver executable on Windows
DEFAULT_WINDOWS_CHROME_DRIVER_PATH = 'libs/chromedriver.exe'

# Default path to the Chrome WebDriver executable on Ubuntu
DEFAULT_UBUNTU_CHROME_DRIVER_PATH = '/usr/lib/chromium-browser/chromedriver'

# Directory path to store cookies
COOKIE_FILE_DIR = 'data/input/cookies.ck'

# URL to fetch cookies (e.g., for Facebook login)
COOKIE_URL = 'https://www.facebook.com'

# Base URL for web scraping
BASE_URL = 'https://www.facebook.com'

# Directory path to store log files
LOG_FILE_DIR = 'data/output/logs/debug.log'
