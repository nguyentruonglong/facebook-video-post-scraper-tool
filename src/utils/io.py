import json
import sys
from datetime import datetime

import pytz
import settings
from fake_useragent import UserAgent

class DataIO:
    def get_current_time(self):
        """
        Get the current time in the specified timezone.

        Returns:
            datetime: Current time with timezone information.
        """
        return datetime.now(pytz.timezone('UTC')).astimezone(pytz.timezone(
            settings.TIMEZONE))

    def write_to_log(self, contents):
        """
        Write log contents to the log file with a timestamp.

        Args:
            contents (str): The contents to be written to the log.

        Returns:
            None
        """
        try:
            # Get the log file path from settings
            log_filepath = settings.LOG_FILE_DIR
            # Open the log file in append mode with UTF-8 encoding
            log_file = open(log_filepath, 'a+', encoding='utf8')
            # Get the current datetime in the specified timezone
            current_datetime = datetime.now().astimezone(pytz.timezone(
                settings.TIMEZONE))
            # Format the datetime as a string
            formatted_datetime = current_datetime.strftime(
                '%Y-%m-%d %H:%M:%S.%f')
            # Format the log entry with timestamp
            log_entry = f'{formatted_datetime} | {contents}\n'
            # Print the log entry
            print(log_entry)
            # Write the log entry to the log file
            log_file.write(log_entry)
        except Exception as e:
            # Handle any exceptions that occur during log file operations
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
        finally:
            # Close the log file
            log_file.close()

    def generate_user_agent(self):
        """
        Generate a user agent string.

        Returns:
            str: A user agent string.
        """
        try:
            # Create a UserAgent generator
            user_agent_generator = UserAgent()
            # Get a user agent string for Google Chrome
            user_agent = user_agent_generator['google chrome']
        except Exception as e:
            # Handle any exceptions that occur during user agent generation
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
            # Default to an empty string in case of an error
            user_agent = str()
        finally:
            # Return the generated user agent
            return user_agent

    def load_cookies(self, cookie_filepath):
        """
        Load cookies from a JSON file.

        Args:
            cookie_filepath (str): Path to the JSON file containing cookies.

        Returns:
            dict: Loaded cookies.
        """
        try:
            # Open the cookie JSON file for reading with UTF-8 encoding
            cookie_file = open(cookie_filepath, 'rt', encoding='utf-8')
            # Read the contents of the cookie file
            cookie_contents = cookie_file.read()
            # Parse the JSON contents into a dictionary
            cookies = json.loads(cookie_contents)
        except Exception as e:
            # Handle any exceptions that occur during cookie loading
            cookies = dict()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            print(error_message)
        finally:
            # Close the cookie file and return the loaded cookies
            cookie_file.close()
            return cookies
